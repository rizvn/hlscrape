package app;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Riz
 */
public class ScraperTest{
  
  @Test
  public void outputReport() throws Exception
  {
    Injector injector  = Guice.createInjector(new Config());
    Scraper scraper    = injector.getInstance(Scraper.class);

    Stock stock = new Stock();
    stock.setMarketCap(1.0);
    stock.setMainPageUrl("urla");
    stock.setName("ABC");
    stock.setPrice(1.69);
    stock.setTodaysChange(1.05);
    stock.setNetAssets(1.05);
    stock.setBalanceSheetTimeStamp("12/05/2015");
    stock.setFinancialsPage("urlb");
    stock.setOneWeekChange(1.0);
    stock.setOneMonthChange(2.0);
    stock.setThreeMonthChange(3.0);
    stock.setSixMonthChange(4.0);
    stock.setSixMonthChange(5.0);
    stock.setOneYearChange(5.0);
    stock.setTwoYearChange(5.0);
    stock.setThreeYearChange(5.0);
    stock.setFiveYearChange(6.0);
    stock.setPe(10.0);
    stock.setBookValuePerShare(2.0);
    stock.setSymbol("ABC");

    List<Stock> stocks = Arrays.asList(stock);

    scraper.outputReport("ftse250.html", stocks);
  }

  @Test
  public void getBookValuePerShare() throws Exception {
    Stock stock = new Stock();
    stock.setSymbol("bez");
    Scraper scraper = new Scraper();
    scraper.getKeyRatios(stock);
    Assert.assertNotNull(stock.getBookValuePerShare());
  }

  @Test
  public void getFinancials() throws Exception{
    Stock stock = new Stock();
    stock.setFinancialsPage("http://www.hl.co.uk/shares/shares-search-results/g/grafton-group-unit-1-ord-1-c-ord-and-17-as/financial-statements-and-reports");
    Scraper scraper = new Scraper();
    scraper.getFinancials(stock);
    Assert.assertNotNull(stock.getNetAssets());
    Assert.assertNotNull(stock.getBalanceSheetTimeStamp());
  }

  @Test
  public void getFundamentals() throws Exception{
    Stock stock = new Stock();
    stock.setMainPageUrl("http://www.hl.co.uk/shares/shares-search-results/B00MZ44");
    Scraper scraper = new Scraper();
    scraper.getFundamentals(stock);
    Assert.assertNotNull(stock.getPe());
    Assert.assertNotNull(stock.getMarketCap());
    Assert.assertNotNull(stock.getFinancialsPage());
  }

  @Test
  public void getStocks() throws Exception{
    Scraper scraper = new Scraper();
    List<Stock> stocks = scraper.getStocks("http://www.hl.co.uk/shares/stock-market-summary/ftse-250/performance?column=date_5y&order=desc");
    Assert.assertFalse(stocks.isEmpty());
    System.out.println(stocks);
  }

  @Test
  public void generateFtse250Report() throws Exception{
    Injector injector  = Guice.createInjector(new Config());
    Scraper scraper    = injector.getInstance(Scraper.class);
    List<Stock> stocks = scraper.getStocks("http://www.hl.co.uk/shares/stock-market-summary/ftse-250/performance?column=date_5y&order=desc");
    scraper.outputReport("ftse250.html", stocks);
  }

}