package app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Riz
 */
public class MiscTest{

  class Ball{
    String color;
    String size;

    public Ball(String color, String size){
      this.color = color;
      this.size = size;
    }

    @Override
    public String toString(){
      return String.format("[Ball color: %s, size: %s]", color, size);
    }
  }

  class GroupBySize implements Collector<Ball, Map<String, List<Ball>>, List<List<Ball>>>{

    @Override
    public Supplier<Map<String, List<Ball>>> supplier(){
      //1. instantiate the map
      return HashMap::new;
    }

    @Override
    public BiConsumer<Map<String, List<Ball>>, Ball> accumulator(){
      //2. put each ball in its bucket (list) based on size
      return (map, ball) -> {
        List<Ball> balls = map.getOrDefault(ball.size, new ArrayList<>());
        balls.add(ball);
        map.put(ball.size, balls);
      };
    }

    @Override
    public BinaryOperator<Map<String, List<Ball>>> combiner(){
      //3. combiner used for parrallel stream's only, used to merge results from 2 accumulators
      return (leftMap, rightMap) -> {
        rightMap.entrySet().forEach(rightKey -> {
          if(leftMap.containsKey(rightKey.getKey())){
            List<Ball> balls = leftMap.get(rightKey.getKey());
            balls.addAll(rightKey.getValue());
          }
        });
        return rightMap;
      };
    }

    @Override
    public Function<Map<String, List<Ball>>, List<List<Ball>>> finisher(){
      //return each list in map, collected in another list
      return map ->  map.entrySet()
                        .stream()
                        .map(entry -> entry.getValue())
                        .collect(Collectors.toList());
    }

    @Override
    public Set<Characteristics> characteristics()
    {
      //finish in the same order as input
      return new HashSet<>(Arrays.asList(Characteristics.IDENTITY_FINISH));
    }
  }

  @Test
  public void testCollector(){
    List<Ball> balls = new ArrayList<>();
    List<List<Ball>> bags = balls.stream().collect(new GroupBySize());
    Assert.assertNotNull(bags);

  }
}
