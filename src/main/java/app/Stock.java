package app;

/**
 * Created by Riz
 */
public class Stock
{
  String name;
  String symbol;
  String mainPageUrl;
  String financialsPage;
  Double oneWeekChange;
  Double oneMonthChange;
  Double threeMonthChange;
  Double sixMonthChange;
  Double oneYearChange;
  Double twoYearChange;
  Double threeYearChange;
  Double fiveYearChange;
  Double pe;
  Double marketCap;
  Double netAssets;
  Double todaysChange;
  Double price;
  Double bookValuePerShare;
  String balanceSheetTimeStamp;

  public String getName()
  {
    return name;
  }

  public void setName(String aName)
  {
    name = aName;
  }

  public String getSymbol()
  {
    return symbol;
  }

  public void setSymbol(String aSymbol)
  {
    symbol = aSymbol;
  }

  public String getMainPageUrl()
  {
    return mainPageUrl;
  }

  public void setMainPageUrl(String aMainPageUrl)
  {
    mainPageUrl = aMainPageUrl;
  }

  public String getFinancialsPage()
  {
    return financialsPage;
  }

  public void setFinancialsPage(String aFinancialsPage)
  {
    financialsPage = aFinancialsPage;
  }

  public Double getOneWeekChange()
  {
    return oneWeekChange;
  }

  public void setOneWeekChange(Double aOneWeekChange)
  {
    oneWeekChange = aOneWeekChange;
  }

  public Double getOneMonthChange()
  {
    return oneMonthChange;
  }

  public void setOneMonthChange(Double aOneMonthChange)
  {
    oneMonthChange = aOneMonthChange;
  }

  public Double getThreeMonthChange()
  {
    return threeMonthChange;
  }

  public void setThreeMonthChange(Double aThreeMonthChange)
  {
    threeMonthChange = aThreeMonthChange;
  }

  public Double getOneYearChange()
  {
    return oneYearChange;
  }

  public void setOneYearChange(Double aOneYearChange)
  {
    oneYearChange = aOneYearChange;
  }

  public Double getTwoYearChange()
  {
    return twoYearChange;
  }

  public void setTwoYearChange(Double aTwoYearChange)
  {
    twoYearChange = aTwoYearChange;
  }

  public Double getThreeYearChange()
  {
    return threeYearChange;
  }

  public void setThreeYearChange(Double aThreeYearChange)
  {
    threeYearChange = aThreeYearChange;
  }

  public Double getFiveYearChange()
  {
    return fiveYearChange;
  }

  public void setFiveYearChange(Double aFiveYearChange)
  {
    fiveYearChange = aFiveYearChange;
  }

  public Double getMarketCap()
  {
    return marketCap;
  }

  public void setMarketCap(Double aMaketCap)
  {
    marketCap = aMaketCap;
  }

  public Double getNetAssets()
  {
    return netAssets;
  }

  public void setNetAssets(Double aNetAssets)
  {
    netAssets = aNetAssets;
  }

  public String getBalanceSheetTimeStamp()
  {
    return balanceSheetTimeStamp;
  }

  public void setBalanceSheetTimeStamp(String aBalanceSheetTimeStamp)
  {
    balanceSheetTimeStamp = aBalanceSheetTimeStamp;
  }

  public Double getSixMonthChange()
  {
    return sixMonthChange;
  }

  public void setSixMonthChange(Double aSixMonthChange)
  {
    sixMonthChange = aSixMonthChange;
  }

  public Double getPe()
  {
    return pe;
  }

  public void setPe(Double aPe)
  {
    pe = aPe;
  }

  public Double getTodaysChange()
  {
    return todaysChange;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setTodaysChange(Double aTodaysChange)
  {
    todaysChange = aTodaysChange;
  }

  public void setPrice(Double aPrice)
  {
    price = aPrice;
  }

  public Double getBookValuePerShare() {
    return bookValuePerShare;
  }

  public void setBookValuePerShare(Double bookValuePerShare) {
    this.bookValuePerShare = bookValuePerShare;
  }
}
