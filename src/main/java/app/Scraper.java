package app;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
 * Created by Riz
 */
@Singleton
public class Scraper
{
  Freemarker mFreemarker;

  public List<Stock> getStocks(String pageUrl){
    try{
      URL url = new URL(pageUrl);
      Document doc = Jsoup.parse(url, 10000);
      final Elements stockRows = doc.select("table.table-styled tbody tr.stock_row");

      List<Stock> stocks = stockRows.parallelStream().map(element -> {
        Elements tds = element.getElementsByTag("td");
        Stock stock = new Stock();
        stock.setSymbol(tds.get(0).text());
        stock.setName(tds.get(1).text());
        stock.setMainPageUrl(tds.get(1).getElementsByTag("a").get(0).attr("href"));
        stock.setOneWeekChange(asDouble(tds.get(2).text()));
        stock.setOneMonthChange(asDouble(tds.get(3).text()));
        stock.setThreeMonthChange(asDouble(tds.get(4).text()));
        stock.setSixMonthChange(asDouble(tds.get(5).text()));
        stock.setOneYearChange(asDouble(tds.get(6).text()));
        stock.setTwoYearChange(asDouble(tds.get(7).text()));
        stock.setThreeYearChange(asDouble(tds.get(8).text()));
        stock.setFiveYearChange(asDouble(tds.get(9).text()));
        getFundamentals(stock); //has to go first to get financials page link
        getFinancials(stock);
        getKeyRatios(stock);
        return stock;
      }).collect(Collectors.toList());

      return stocks;
    }
    catch (Exception ex){
      throw new IllegalStateException(ex);
    }
  }

  protected void getFinancials(Stock aStock){
    try{
      Document financialsPage = Jsoup.parse(new URL(aStock.getFinancialsPage()), 10000);
      Elements finTable = financialsPage.select("table.factsheet-table");

      Elements balanceSheetHeaders = finTable.select("table.factsheet-table tr.factsheet-head th");

      if(balanceSheetHeaders.size() > 1){
        String balancesheetDate = balanceSheetHeaders.get(1).text();
        aStock.setBalanceSheetTimeStamp(balancesheetDate);

        Elements rows = financialsPage.select("table.factsheet-table tr");
        rows.forEach(row -> {
          Elements tds = row.getElementsByTag("td");
          if (tds.size() > 0) {
            if (tds.get(0).text().contains("Net Assets")){
              aStock.setNetAssets(asDouble(tds.get(1).text()));
            }
          }
        });
      }
    }catch (Exception ex){
      ex.printStackTrace();
      //throw new IllegalStateException(ex);
    }
  }

  protected void getFundamentals(Stock aStock){
    try{
      Document mainPage = Jsoup.parse(new URL(aStock.getMainPageUrl()), 10000);
      Elements cols = mainPage.select("#security-detail .columns");
      cols.forEach( col -> {
        //Get market cap value
        if(col.text().trim().contains("Market capitalisation")){
          Integer multiplier = col.text().contains("bn")? 1000 : 1; //mulitply by 1000 for million, so everything is in millions
          String mkCapText   = col.text().replaceAll("[^0-9.]", ""); //remove non numeral or period
          mkCapText          = mkCapText.startsWith(".") ? mkCapText.substring(1) : mkCapText;
          Double mkCap = asDouble(mkCapText) * multiplier;
          aStock.setMarketCap(mkCap);
        }

        //get PE Value
        else if (col.text().contains("P/E ratio")){
          String peText   = col.text().replaceAll("[^0-9.]", "");
          aStock.setPe(asDouble(peText));
        }
      });

      Elements navLinks = mainPage.select("#factsheet-nav-container a");
      navLinks.forEach(navLink -> {
        //get financial page url
        if(navLink.text().contains("Financials")){
          aStock.setFinancialsPage(navLink.attr("href"));
        }
      });

      //todays percentage change
      String percentageChanged = mainPage.select(".change-divide .change[data-field=perc]").get(0).text();
      percentageChanged = percentageChanged.replaceAll("[^0-9.]", "");
      aStock.setTodaysChange(asDouble(percentageChanged));

      //ask price
      String askPrice = mainPage.select("span.ask[data-field=ask]").get(0).text();
      askPrice = askPrice.replaceAll("[^0-9.]", "");
      aStock.setPrice(asDouble(askPrice));
    }
    catch (Exception ex){
      ex.printStackTrace();
      //throw new IllegalStateException(ex);
    }
  }

  public void outputReport(String reportName, List<Stock> stocks){
    Map<String, Object> model = new HashMap<>();
    model.put("stocks", stocks);
    String view = mFreemarker.render("stocksTable", model);

    try{
      FileUtils.forceMkdir(new File("report"));
      IOUtils.write(view.getBytes(StandardCharsets.UTF_8), new FileOutputStream("report/" + reportName));
    }catch (Exception ex){
      throw new IllegalStateException(ex);
    }
  }

  public Double asDouble(String text){
    if(StringUtils.isBlank(text)){
      return null;
    }
    else if(text.trim().startsWith("n")){
      return null;
    }
    else {
      text = text.replace(",", "");
      text = text.replace("(", "");
      text = text.replace(")", "");
      return Double.parseDouble(text);
    }
  }

  public void getKeyRatios(Stock stock){
    try{
      String symbol = stock.getSymbol();
      URL url = new URI("http://financials.morningstar.com/ajax/exportKR2CSV.html?t=xlon:"+ symbol).toURL();
      InputStreamReader response = new InputStreamReader(url.openStream());
      CSVParser csv = CSVFormat.EXCEL.parse(response);
      csv.forEach( row -> {
        String rowLabel = row.get(0);

        if(rowLabel.contains("Book Value Per Share")){
          stock.setBookValuePerShare(Double.parseDouble(row.get(row.size() - 1)));
        }

        //other key ratios here
      });
    }
    catch(Exception ex){
      ex.printStackTrace();
      //throw new IllegalStateException(ex);
    }
  }

  public Freemarker getFreemarker()
  {
    return mFreemarker;
  }

  @Inject
  public void setFreemarker(Freemarker aFreemarker)
  {
    mFreemarker = aFreemarker;
  }
}
