package app;

import static spark.Spark.externalStaticFileLocation;
import static spark.Spark.get;
import static spark.Spark.port;

/**
 * Created by Riz
 */
public class Web {

  public static void main(String [] args){
    port(8080);
    externalStaticFileLocation("report");

    get("/", (req, res) -> {
      res.redirect("/ftse250.html");
      return "";
    });

  }
}
