package app;

import com.google.inject.Singleton;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Riz
 */
@Singleton
public class Freemarker
{

  Configuration configuration;

  public Freemarker(){
    configuration = new Configuration();
    configuration.setClassForTemplateLoading(Freemarker.class, "/views");
    configuration.setEncoding(Locale.ENGLISH, StandardCharsets.UTF_8.name());
  }
  
  public String render(String view){
    return render(view, new HashMap<String, Object>());
  }


  public String render(String view, Map<String, Object> model){
    try {
      Template template = configuration.getTemplate(view + ".ftl");
      StringWriter writer = new StringWriter();
      template.process(model, writer);
      return writer.toString();
    }
    catch (Exception ex){
      ex.printStackTrace();
      throw new IllegalStateException(ex);
    }
  }
}
