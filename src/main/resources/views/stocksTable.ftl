<#macro getClass val>
  <#if val?has_content>
    <#if val > 0>
      class="blue"
    <#else >
      class="red"
    </#if>
  </#if>
</#macro>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.3/dt-1.10.12/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.3/dt-1.10.12/datatables.min.js"></script>
    <style>
      body{
        font-family: sans-serif;
      }
      .blue{
        color: blue;
      }

      .red{
        color: red;
      }
    </style>
  </head>
  <body>
  <table id="dataTable" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
      <th>Name</th>
      <th>Symbol</th>
      <th>Price</th>
      <th>Change</th>
      <th>1W</th>
      <th>1M</th>
      <th>3M</th>
      <th>6M</th>
      <th>1Y</th>
      <th>2Y</th>
      <th>3Y</th>
      <th>5Y</th>
      <th>PE</th>
      <th>MktCap</th>
      <th>NetAssets(M)</th>
      <th>BalanceDate</th>
    </tr>
    </thead>
    <tbody>
      <#list stocks as stock>
      <tr>
        <td>${stock.name!}</td>
        <td >${stock.symbol!}</td>
        <td >${stock.price!}</td>
        <td>${stock.todaysChange!}</td>
        <td>${stock.oneWeekChange!}</td>
        <td>${stock.oneMonthChange!}</td>
        <td>${stock.threeMonthChange!}</td>
        <td>${stock.sixMonthChange!}</td>
        <td>${stock.oneYearChange!}</td>
        <td>${stock.twoYearChange!}</td>
        <td>${stock.threeYearChange!}</td>
        <td>${stock.fiveYearChange!}</td>
        <td>${stock.pe!}</td>
        <td>${stock.marketCap!}</td>
        <td>${stock.netAssets!}</td>
        <td>${stock.bookValuePerShare!}</td>
        <td>${stock.balanceSheetTimeStamp!}</td>
      </tr>
      </#list>
    </tbody>
  </table>

  <script>
    $(document).ready(function() {
      $('#dataTable').DataTable();
    });
  </script>
  </body>
</html>